package com.example.gelala.sample;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class AddListView extends AppCompatActivity {
    Button add_ton, btn_sav, btn_can;
    EditText type_name, edit;
    ListView view_name;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrayList;
    AlertDialog alertDia;
    AlertDialog.Builder builder;
    LayoutInflater inflater;
    TextView title;
    View dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_list_view);

        add_ton = (Button) findViewById(R.id.addton);
        type_name = (EditText) findViewById(R.id.typename);
        view_name = (ListView) findViewById(R.id.viewname);
        arrayList = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, arrayList);
        view_name.setAdapter(adapter);
        builder = new AlertDialog.Builder(AddListView.this);
        inflater = AddListView.this.getLayoutInflater();
        dialog = inflater.inflate(R.layout.activity_dialog, null);
        builder.setView(dialog);
        title = (TextView) dialog.findViewById(R.id.typename);
        title.setText(arrayList.getClass().toString());
        edit = (EditText) dialog.findViewById(R.id.editname);

        alertDia = builder.create();
        //ad.show();
        alertDia.setCancelable(false);

        //Button btn_minus = (Button) dialog.findViewById(R.id.buttondialog_minus);
        //Button btn_plus = (Button) dialog.findViewById(R.id.buttondialog_plus);
        btn_sav = (Button) dialog.findViewById(R.id.savbtn);
        btn_can = (Button) dialog.findViewById(R.id.canbtn);
        btn_sav.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Toast.makeText(getApplicationContext(), "Whatevs.", Toast.LENGTH_LONG).show();
            }
        });

        btn_can.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                finish();
            }
        });


        //add_ton.set

            /*builder.setMessage("Hello Dialog");
            builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int save) {
                    Toast.makeText(getApplicationContext(), "Whatevs.", Toast.LENGTH_LONG).show();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int cancel) {
                    finish();
                }
            });
            //alertDia = builder.create();
           // Intent alrt = new Intent(this, Dialog.class);
            alertDia.show();*/

        add_ton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type_name != null) {
                    arrayList.add(type_name.getText().toString());
                    adapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(getApplicationContext(), "Please enter a name.", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_list_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
