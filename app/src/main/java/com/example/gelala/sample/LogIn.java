package com.example.gelala.sample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.jar.Attributes;

public class LogIn extends AppCompatActivity {
    ArrayList<String> namesList;
    EditText email_add;
    EditText pass_word;
    Intent login;
    Button sign_in;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        namesList = new ArrayList<String>();
            namesList.add("ADMIN");
            namesList.add("GELA");
            namesList.add("JAMES");
            namesList.add("TIN");

        email_add = (EditText) findViewById(R.id.email);
        pass_word = (EditText) findViewById(R.id.password);

            sign_in = (Button) findViewById(R.id.signin);
            sign_in.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (email_add != null && pass_word != null) {
                        if(namesList.contains(email_add.getText().toString()) && pass_word.getText().toString().equals("admin")){
                            //Toast.makeText(getApplicationContext(), "Redirecting...", Toast.LENGTH_LONG).show();
                            login = new Intent(LogIn.this, MainActivity.class);
                            login.putExtra("username", email_add.getText().toString());
                            startActivity(login);
                            finish();
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "Wrong Credentials. Enter email and password again.", Toast.LENGTH_LONG).show();
                        }
                    }

                    else {
                        Toast.makeText(getApplicationContext(), "Please enter email and password.", Toast.LENGTH_LONG).show();
                    }

                }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_log_in, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
