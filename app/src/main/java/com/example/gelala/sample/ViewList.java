package com.example.gelala.sample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class ViewList extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_list);
        ListView view = (ListView) findViewById(R.id.students);
        String [] value = new String[]{"Gynber Aguelo", "Gloyd Castillo", "April Espelita", "Cres Faustinorio", "Clark Garde",
            "Alyssa Javelosa", "James Manaloto", "Ace Penaranda", "Crist Sermonia", "Viatrice Vargas"};
        ArrayAdapter<String> adapt = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, value);
        view.setAdapter(adapt);

        Button click_act = (Button) findViewById(R.id.clickact);
        click_act.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent hey = new Intent(ViewList.this, AddListView.class);
               startActivity(hey);
            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
